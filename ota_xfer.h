/**
  *@file ota_xfer.h
  *@brief header for 
  *@author jason
  *@date 07/24/23
  */

#pragma once
#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include "ota_img.h"
#include "Utilities/CRC/crc32.h"
#include <stdint.h>


/* Exported constants --------------------------------------------------------*/
#define OTA_XFER_DEFAULT_RETRY_COUNT 3
#define OTA_XFER_NO_MISSING_BLOCKS -1

#define OTA_XFER_STATE_IDLE      0    //Waiting for start
#define OTA_XFER_STATE_STARTED   1    //Started, waiting for internal setup
#define OTA_XFER_STATE_BULK      2    //Bulk transfer, sender can send blocks at will
#define OTA_XFER_STATE_CLEANUP   3    //Cleanup phase, receiver uses blockMask to request missing blocks
#define OTA_XFER_STATE_FINISHED  4    //Transfer complete




/* Exported types ------------------------------------------------------------*/
typedef struct{
	uint32_t size;                //Size of image in bytes 
	uint32_t blockCount;          //Number of blocks in image
	uint32_t crc;                 //CRC of image
	uint8_t* blockMask;           //Bitmask of blocks received
	uint32_t blockMaskLen;        //Length of bitmask in bytes
	int32_t lastRequestedAddr;    //Last block requested
  ota_img_mgr_t* img_mgr;       //Pointer to OTA Image Manager
  ota_partition_t* part;        //Pointer to partition
  crc32_ctx_t crc_ctx;          //CRC context
	uint8_t state;                //State of transfer
  uint8_t retryCount;           //Number of retries
} ota_xfer_t;


/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */


void ota_xfer_init(ota_xfer_t* xfer); 

/**
 * @brief Deinitialize OTA image transfer struct
 * 
 * @param xfer 
 */
void ota_xfer_deinit(ota_xfer_t* xfer);

/**
 * @brief Initialize OTA image transfer
 * 
 * @param xfer  Pointer to OTA transfer structure
 * @param label label of partition to transfer to
 * @param strVersion string version of new image 
 * @param size size of new image in bytes 
 * @param crc crc32 of new image 
 */
ota_status_t ota_xfer_start(ota_xfer_t* xfer, ota_img_mgr_t* mgr, const char* label, const char* strVersion, uint32_t size, uint32_t crc);

/**
 * @brief Set the state of the OTA transfer
 * 
 * @param xfer ptr to OTA transfer struct
 * @param state new state
 */
void ota_xfer_set_state(ota_xfer_t* xfer, uint8_t state);

/**
 * @brief Resets the block mask to all zeros. This will cause it to request all blocks again
 * 
 * @param xfer 
 */
void ota_xfer_retry(ota_xfer_t* xfer);


/**
 * @brief Write a block of data to the OTA transfer
 * 
 * @param xfer ptr to OTA transfer struct
 * @param blockNum block number to write
 * @param data ptr to data to write
 * @param len length of data to write
 * @return number of bytes written
 */
uint32_t ota_xfer_write_block(ota_xfer_t* xfer, uint32_t addr, uint8_t* data, uint32_t len);

/**
 * @brief Get the next missing block number
 * 
 * @param xfer ptr to OTA transfer struct
 * @return int next missing block number, or -1 if all blocks received
 */
int ota_xfer_get_next_missing_block(ota_xfer_t* xfer);

/**
 * @brief Verifies the image
 * 
 * @param xfer xfer job to verify
 * @return true if verified 
 */
ota_status_t ota_xfer_verify_img(ota_xfer_t* xfer);



#ifdef __cplusplus
}
#endif



