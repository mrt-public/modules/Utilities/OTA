#ifdef UNIT_TESTING_ENABLED

extern "C" {
    #include "ota_img.c"
    #include "ota_xfer.c"
}

#include <gtest/gtest.h>

#define STAGING_SIZE 8000
#define PROGMEM_SIZE 8000


ota_img_mgr_t img_mgr;

ota_dsk_t* stagingDsk;
ota_dsk_t* progmemDsk;


uint8_t stagingData[STAGING_SIZE] = {0};
uint8_t progmemData[PROGMEM_SIZE] = {0};

mrt_status_t staging_read(uint32_t offset, uint8_t *data, uint32_t size)
{
    memcpy(data, stagingData + offset, size);
    return MRT_STATUS_OK;
}

mrt_status_t staging_write(uint32_t offset, uint8_t *data, uint32_t size)
{
    memcpy(stagingData + offset, data, size);
    return MRT_STATUS_OK;
}

mrt_status_t progmem_read(uint32_t offset, uint8_t *data, uint32_t size)
{
    memcpy(data, progmemData + offset, size);
    return MRT_STATUS_OK;
}

mrt_status_t progmem_write(uint32_t offset, uint8_t *data, uint32_t size)
{
    memcpy(progmemData + offset, data, size);
    return MRT_STATUS_OK;
}




TEST(OTA_Test, init )
{

    memset(stagingData, 0, STAGING_SIZE);
    memset(progmemData, 0, PROGMEM_SIZE);

    //Initialize data (should be blank)
    ota_img_mgr_init(&img_mgr);
    stagingDsk = ota_img_mgr_add_dsk(&img_mgr, "staging", 0, staging_write, staging_read,NULL);
    progmemDsk = ota_img_mgr_add_dsk(&img_mgr, "progmem", 0, progmem_write, progmem_read,NULL);


    //Check that the staging area is blank
    ASSERT_EQ(stagingDsk->partitionCount,0); //No partitions

    //Add in partitions 
    ota_dsk_add_partition(stagingDsk, OTA_PARTITION_TABLE_ALIGNMENT * 4 , 512, "part1");
    ota_dsk_add_partition(stagingDsk, OTA_PARTITION_ADDR_AUTO, 512, "part2");
    ota_dsk_add_partition(stagingDsk, OTA_PARTITION_ADDR_AUTO, 512, "part3");

    ota_dsk_commit_partition_table(stagingDsk);


    //Initialize a new dsk and make sure it reads the partitions
    ota_dsk_t readBack;

    ota_dsk_init(&readBack,"test", 0, staging_write, staging_read,NULL);

    //Check that the staging area has the partitions
    ASSERT_EQ(readBack.partitionCount,3); //check partitions

}

TEST(OTA_Test, staging)
{
    crc32_ctx_t crc_ctx;
    ota_partition_t* part1 = ota_dsk_get_partition(stagingDsk, "part1");

    uint8_t data[148] = {0};

    for(int i = 0; i < 148; i++)
    {
        data[i] = i;
    }

    //Get CRC of data
    crc32_init(&crc_ctx);
    crc32_update(&crc_ctx, data, 148);
    uint32_t crc_result = crc32_result(&crc_ctx);

    part1->image.size = 148;
    part1->image.crc = crc_result;


    //Commit the partition
    ota_dsk_commit_partition_table(stagingDsk);

    //Write image data 
    ota_partition_write_image(part1, 0, data, 148);

}

TEST(OTA_Test, ApplyUpdate)
{
    ota_partition_t* part1 = ota_dsk_get_partition(stagingDsk, "part1");

    //Apply the update
    ota_partition_copy_to_dsk(part1, progmemDsk, 4);

    //verify that the data was written to the progmem area at an offset of 4
    for(int i = 0; i < 148; i++)
    {
        ASSERT_EQ(progmemData[i + 4], i);
    }
}


TEST(OTA_XFER, BlockMask)
{

    ota_xfer_t xfer; 

    ota_xfer_init(&xfer);

    img_mgr.blockSize = 256; 

    ota_xfer_start(&xfer, &img_mgr, "staging/part1", "1.0.0", 512, 0); 

    ASSERT_EQ(xfer.blockMask[0], 0); 

    ota_xfer_write_block(&xfer, 0, progmemData, 256); 

    ASSERT_EQ(xfer.blockMask[0], 0x80); 

    
    int next = ota_xfer_get_next_missing_block(&xfer);

    ASSERT_EQ(next, 1);


}








#endif